﻿namespace PaatashalaApi.DTOs
{
    public class ActivityReport
    {
        public long Id { get; set; }
        public string StudentName { get; set; }
        public bool isPresent { get; set; }
    }
}
