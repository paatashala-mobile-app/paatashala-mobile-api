﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PaatashalaApi.DTOs;
using PaatashalaApi.Models;
using System.Globalization;
using System.Security.Cryptography;

namespace PaatashalaApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttandanceController : ControllerBase
    {
        private readonly WebSchoolContext db;
        private long orgId;

        public AttandanceController(WebSchoolContext _db)
        {
           db = _db;
        }
        private DateTime OnlyDateConvertToDateTime(string input)
        {
            DateTime dateTime;
            if (DateTime.TryParse(input, out dateTime))
            {
                return dateTime;
            }
            return DateTime.UtcNow;
        }

        private DateTime ConvertToDateTime(string input)
        {
            DateTime dateTime;
            DateTime dt = DateTime.ParseExact(input.Substring(0, 24),
                              "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            if (DateTime.TryParse(input, out dateTime))
            {
                return dateTime;
            }
            return dt;
        }

        [HttpGet("GetStudentsList")]
        public async Task<JsonResult> GetStudentsList(long OrgId)
        {
            try
            {
                var AdmStudents = await (from tableStudent in db.TblStudents
                                         join tableBatchAdmission in db.TblBatchAdmissions on tableStudent.Id equals tableBatchAdmission.StudentId
                                         where tableBatchAdmission.OrgId == OrgId && tableStudent.IsLead == false
                                         select new
                                         {
                                             tableStudent.FirstName,
                                             tableStudent.MiddleName,
                                             tableStudent.LastName,
                                             tableStudent.Id,
                                             tableStudent.StudentId
                                         })
                                                .ToListAsync();
                var studentList = AdmStudents.Select(s => new
                {
                    Name = $"{s.FirstName} {s.MiddleName} {s.LastName}",
                    s.Id,
                    s.StudentId
                }).ToList();
                return new JsonResult(new { AdmStudents = studentList });
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet("SaveAttendance")]
        public async Task<JsonResult> SaveAttendance(long OrgId, string StudentId, string scanDateTime, bool IsCheckIn)
        {

            {
                try
                {
                    DuplicateAttendance DuplicateAttendanceObj = new DuplicateAttendance();
                    var datee = ConvertToDateTime(scanDateTime);
                    var NowDate = new DateTime(datee.Year, datee.Month, datee.Day);
                    string[] arrlong = null;
                    if (StudentId.Contains(','))
                    {
                        arrlong = StudentId.Split(',');
                    }
                    else
                    {
                        arrlong = new string[] { StudentId };
                    }
                    DuplicateAttendanceObj.DuplicatesList = new List<Dupicates>();

                    if (IsCheckIn == true)
                    {
                        foreach (var temp in arrlong)
                        {
                            long StudId = Convert.ToInt64(temp);
                            var DuplicateCheckIn = db.TblAttendanceHourlies.Where(x => x.OrgId == OrgId && x.StudentId == StudId && x.Date == NowDate && x.IsAttendance == true).OrderByDescending(x => x.Time).Take(1).Select(y => y).FirstOrDefault();
                            if (DuplicateCheckIn == null || DuplicateCheckIn.IsPickUp == false)
                            {
                                db.TblAttendanceHourlies.Add(new TblAttendanceHourly()
                                {
                                    StudentId = Convert.ToInt64(temp),
                                    IsPickUp = IsCheckIn,
                                    IsAttendance = true,
                                    Time = new TimeSpan(datee.Hour, datee.Minute, datee.Second),
                                    Date = new DateTime(datee.Year, datee.Month, datee.Day),
                                    OrgId = OrgId
                                });
                                db.SaveChanges();
                            }
                            else
                            {
                                var StudnetDuplicate = new Dupicates();
                                DuplicateAttendanceObj.DuplicatesList.Add(StudnetDuplicate);
                                StudnetDuplicate.StudentId = Convert.ToInt64(temp);
                                DuplicateAttendanceObj.ReasonDuplicate = "Already Checked In";
                            }
                        }
                        return new JsonResult(new { status = true, DuplicateAttendanceObj });
                    }
                    else
                    {
                        foreach (var temp in arrlong)
                        {
                            long StudId = Convert.ToInt64(temp);
                            var Dup = db.TblAttendanceHourlies.Where(x => x.OrgId == OrgId && x.StudentId == StudId && x.Date == NowDate && x.IsAttendance == true && x.IsPickUp == true).OrderByDescending(x => x.Time).Take(1).Select(y => y).FirstOrDefault();
                            if (Dup != null)
                            {
                                var d = new TimeSpan(Dup.Time.Hours, Dup.Time.Minutes, Dup.Time.Seconds);
                                var e = new TimeSpan(datee.Hour, datee.Minute, datee.Second);
                                TimeSpan f = e.Subtract(d);
                                if (f.Hours >= 2)
                                {
                                    db.TblAttendanceHourlies.Add(new TblAttendanceHourly()
                                    {
                                        StudentId = Convert.ToInt64(temp),
                                        IsPickUp = IsCheckIn,
                                        IsAttendance = true,
                                        Time = new TimeSpan(datee.Hour, datee.Minute, datee.Second),
                                        Date = new DateTime(datee.Year, datee.Month, datee.Day),
                                        OrgId = OrgId
                                    });
                                    db.SaveChanges();
                                }
                                else
                                {
                                    var StudnetDuplicate = new Dupicates();
                                    DuplicateAttendanceObj.DuplicatesList.Add(StudnetDuplicate);
                                    StudnetDuplicate.StudentId = Convert.ToInt64(temp);
                                    DuplicateAttendanceObj.ReasonDuplicate = "Below 2 Hrs Checkin";
                                }
                            }
                            else
                            {
                                var StudnetDuplicate = new Dupicates();
                                DuplicateAttendanceObj.DuplicatesList.Add(StudnetDuplicate);
                                StudnetDuplicate.StudentId = Convert.ToInt64(temp);
                                DuplicateAttendanceObj.ReasonDuplicate = "Not Checkin can't CheckOut";
                            }
                        }
                        return new JsonResult(new { status = true, DuplicateAttendanceObj });
                    }

                }
                catch (Exception e)
                {
                    return new JsonResult(new { status = false, message = e.ToString() });
                }

            }
        }
    }
}
